#ifndef MAIN_H
#define MAIN_H
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
typedef struct{
    char part_status_part;
    char part_type;
    char part_fit;
    int part_start;
    int part_size;
    char part_name[16];
}partition;
typedef struct{
    int mbr_tamanio;
    time_t mbr_fecha_creacion;
    int mbr_disk_signature;
    partition mrb_partition1;
    partition mrb_partition2;
    partition mrb_partition3;
    partition mbr_partition4;
}mbr;
typedef struct{
    char part_status_ebr;
    char part_fit;
    int part_status;
    int part_size;
    int part_next;
    char part_name[16];
}ebr;
typedef struct{
    int size;
    char unit;
    char* path;
}disk;
void to_lower(char* input){
    for(int i = 0; input[i]; i++){
        input[i] = tolower(input[i]);
    }
}
struct stat st = {0};
int cs = 0, cot = 0;
void ver_str(char* input){
    if(strcmp(input, "mkdisk") == 0){
        cs = 2;
    }else if(strcmp(input, "rmdisk") == 0){
        cs = 2;
    }else if(strcmp(input, "fdisk") == 0){
        cs = 3;
    }else if(strcmp(input, "mount") == 0){
        cs = 4;
    }else if(strcmp(input, "unmount") == 0){
        cs = 5;
    }else if(strcmp(input, "rep") == 0){
        cs = 6;
    }else{
        printf("Error: No se reconoce el comando %s", input);
    }
}
int c = 0, d = 0;
disk disc;
void sint_mkdisk(char* input){
    switch(c){
        case 0:
            if(strcmp(input, "-size") == 0){
                c = 1;
                d = 2;
            }else if(strcmp(input, "-unit") == 0){
                c = 1;
                d = 3;
            }else if(strcmp(input, "-path") == 0){
                c = 1;
                d = 4;
            }else{
                printf("Error: No se reconoce el comando %s", input);
            }
        break;
        case 1:
            if(strcmp(input, "=") == 0){
                c = 5;
            }else{
                printf("Error: No se reconoce el comando %s", input);
            }
        break;
        case 2:
            if(strcmp(input, "-unit") == 0){
                c = 1;
                d = 8;
            }else if(strcmp(input, "-path") == 0){
                c = 1;
                d = 7;
            }else{
                printf("Error: No se reconoce el comando %s", input);
            }
        break;
        case 3:
            if(strcmp(input, "-size") == 0){
                c = 1;
                d = 7;
            }else if(strcmp(input, "-path") == 0){
                c = 1;
                d = 6;
            }else{
                printf("Error: No se reconoce el comando %s", input);
            }
        break;
        case 4:
            if(strcmp(input, "-size") == 0){
                c = 1;
                d = 7;
            }else if(strcmp(input, "-unit") == 0){
                c = 1;
                d = 6;
            }else{
                printf("Error: No se reconoce el comando %s", input);
            }
        break;
        case 5:
            if(d == 2){
                disc.size = input;
            }else if(d == 3){
                disc.path = input;
            }else if(d == 4){
                disc.unit = input;
            }
            c = d;
        break;
        case 6:
            if(strcmp(input, "-size") == 0){
                c = 1;
            }else{
                printf("Error: No se reconoce el comando %s", input);
            }
        break;
        case 7:
            if(strcmp(input, "-unit") == 0){
                c = 1;
            }else{
                printf("Error: No se reconoce el comando %s", input);
            }
        break;
        case 8:
            if(strcmp(input, "-path") == 0){
                c = 1;
            }else{
                printf("Error: No se reconoce el comando %s", input);
            }
        break;
    }
}

void append_char(char *token, char c)
{
    char temp[2];
    temp[0] = c;
    temp[1] = '\0';
    strcat(token, temp);
}

void create_file(char* path, int size){
    FILE * file  = fopen(path,"w+");
    if (file == NULL){
        create_dir(path);
    }else{
        char cont[size];
        cont[0] = '\0';
        fprintf(file, "%s", cont);
    }
    fclose(file);
}

void create_dir(char* path){
    if (stat(path, &st) == -1) {
        mkdir(path, 0700);
    }
}

int main(void)
{
    int op = 0, state = 0, com = 0;
    char input[1024] = "";
    char token[1024] = "";
    do{
        printf("Ingrese un comando: ");
        fgets(input, 1024, stdin);
        int n = strlen(input)-1;
        if(input[n] == '\n'){
            input[n] = '\0';
        }
        to_lower(input);
        printf("A minusculas: %s\n", input);
        for(int i = 0; i < input[i]; i++){
            char c = input[i];
            switch(state){
                case 0:
                    if(isalpha(c) | c == "-"){
                        append_char(token, c);
                    }else if(isdigit(c)){
                        append_char(token, c);
                    }else if(isspace(c)){
                        state = 1;
                    }else if(c = "\""){
                        state = 2;
                    }else if(c == "="){
                        state = 3;
                    }
                break;
                case 1:
                    state = 0;
                    token[0] = '\0';
                break;
                case 2:
                    if(c = "\""){
                        state = 0;
                    }
                    break;
                case 3:
                    sint(token, cs);
                break;
            }
        }
    }while(op == 0);
    return 0;
}


#endif // MAIN_H
